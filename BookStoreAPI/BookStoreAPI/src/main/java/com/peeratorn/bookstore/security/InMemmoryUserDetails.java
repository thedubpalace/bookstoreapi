package com.peeratorn.bookstore.security;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.peeratorn.bookstore.entity.UserDetails;

public class InMemmoryUserDetails {

	static Map<String, UserDetails> userDetailsMap = Collections.synchronizedMap(new HashMap<>());

	public String save(final UserDetails user) {
		String token = generateToken();
		userDetailsMap.put(token, user);
		return token;
	}

	private String generateToken() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		return bytes.toString();
	}

	public Optional<UserDetails> find(final String token) {
		return Optional.ofNullable(userDetailsMap.get(token));
	}

	public Optional<UserDetails> delete(final String token) {
		return Optional.ofNullable(userDetailsMap.remove(token));
	}
}
