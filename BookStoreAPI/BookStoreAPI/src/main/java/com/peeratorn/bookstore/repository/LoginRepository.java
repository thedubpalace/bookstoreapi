package com.peeratorn.bookstore.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.peeratorn.bookstore.entity.UserDetails;

@Transactional(readOnly = true)
public interface LoginRepository extends CrudRepository<UserDetails, Long> {
    Optional<UserDetails> findByUsername(String username);
}
