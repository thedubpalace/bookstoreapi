package com.peeratorn.bookstore.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.peeratorn.bookstore.entity.User;
import com.peeratorn.bookstore.entity.UserDetails;
import com.peeratorn.bookstore.service.AuthenticationService;
import com.peeratorn.bookstore.service.UserService;

@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	UserService userService;
	
	@Autowired
	AuthenticationService authenService;
	
	@GetMapping()
	@ResponseBody
	public ResponseEntity<?> getUser(@RequestHeader(value = "token") String token) {
		UserDetails userDetails = authenService.validate(token);
		System.out.println(userDetails);
		if (userDetails!= null) {
			return ResponseEntity.status(HttpStatus.OK).body(userDetails.getUser());
		}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("");
	}

	@PostMapping()
    public ResponseEntity<?> createUser(@Valid @RequestBody User body) {
		User user = userService.saveUser(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(user);
    }
	
	@DeleteMapping()
	public @ResponseBody ResponseEntity<String> deleteUser(@RequestHeader(value = "token") String token) {
		authenService.logoff(token);
		return ResponseEntity.status(HttpStatus.OK).body("");
	}
	
}
