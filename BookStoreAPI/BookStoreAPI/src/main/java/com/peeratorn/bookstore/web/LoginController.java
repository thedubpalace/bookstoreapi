package com.peeratorn.bookstore.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.peeratorn.bookstore.entity.UserDetails;
import com.peeratorn.bookstore.service.AuthenticationService;

@RestController
@RequestMapping("/login")
public class LoginController {
	@Autowired
	AuthenticationService authenticationService;

	@PostMapping()
    public ResponseEntity<?> login(@Valid @RequestBody UserDetails body) {
		String token = authenticationService.login(body);
		ObjectNode json = JsonNodeFactory.instance.objectNode();
		json.set("token", TextNode.valueOf(token));
        return ResponseEntity.status(HttpStatus.CREATED).body(json);
    }
	
	
}
