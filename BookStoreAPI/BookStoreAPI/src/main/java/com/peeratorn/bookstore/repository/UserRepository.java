package com.peeratorn.bookstore.repository;

import org.springframework.data.repository.CrudRepository;
import com.peeratorn.bookstore.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
