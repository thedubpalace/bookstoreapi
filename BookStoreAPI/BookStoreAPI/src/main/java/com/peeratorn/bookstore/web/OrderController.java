package com.peeratorn.bookstore.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.peeratorn.bookstore.entity.Order;
import com.peeratorn.bookstore.entity.User;
import com.peeratorn.bookstore.entity.UserDetails;
import com.peeratorn.bookstore.service.AuthenticationService;
import com.peeratorn.bookstore.service.CheckoutService;
import com.peeratorn.bookstore.service.UserService;

@RestController
public class OrderController {
	@Autowired
	UserService userService;
	
	@Autowired
	AuthenticationService authenService;
	
	@Autowired
	CheckoutService priceCalculationService;
	
	@PostMapping("/users/order")
    public ResponseEntity<?> postOrder(@RequestHeader(value = "token") String token, @Valid @RequestBody Order order) {
		UserDetails userDetails = authenService.validate(token);
		User user = userDetails.getUser();
		order.setUser(user);
		user.setBooks(order);
		userService.saveUser(user);
		double totalPrice = priceCalculationService.calculatePrice(order);
		ObjectNode json = JsonNodeFactory.instance.objectNode();
		json.set("price", DoubleNode.valueOf(totalPrice));
        return ResponseEntity.status(HttpStatus.CREATED).body(json);
    }
	
	

}
