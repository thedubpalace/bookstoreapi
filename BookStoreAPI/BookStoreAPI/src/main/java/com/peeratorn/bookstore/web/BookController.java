package com.peeratorn.bookstore.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.peeratorn.bookstore.service.BookService;

@RestController
@RequestMapping("/books")
public class BookController {
	@Autowired
	BookService bookService;

	@GetMapping()
	@ResponseBody
	public ResponseEntity<?> getRecommendedBook() {
		JsonNode recommendedBooks = getBookJsonNode(bookService.getRecommendBooks());
		JsonNode allBooks = getBookJsonNode(bookService.getBooks());
	    ArrayNode response = JsonNodeFactory.instance.arrayNode();

	    List<String> recommentList = new ArrayList<String>();
	    if (recommendedBooks.isArray()) {
	        for (final JsonNode objNode : recommendedBooks) {
	        	recommentList.add(objNode.get("id").asText());
	        }
	    }

		if (allBooks.isArray()) {
			for (final JsonNode node : allBooks) {
				if (recommentList.contains(node.get("id").asText())) {
					response.add(createBookNode(node, BooleanNode.TRUE));
				}
			}
			for (final JsonNode node : allBooks) {
				if (!recommentList.contains(node.get("id").asText())) {
					response.add(createBookNode(node, BooleanNode.FALSE));
				}
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}


	public JsonNode getBookJsonNode(String booksJsonString) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode dataAsMap = null;
		try {
			dataAsMap = mapper.readValue(booksJsonString, JsonNode.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataAsMap;
	}
	
	private ObjectNode createBookNode(JsonNode node, BooleanNode isRecommended) {
		ObjectNode newNode = JsonNodeFactory.instance.objectNode();
		newNode.set("id", node.get("id"));
		newNode.set("name", node.get("book_name"));
		newNode.set("author", node.get("author_name"));
		newNode.set("price", node.get("price"));
		newNode.set("is_recommended", isRecommended);
		return newNode;
	}
}
