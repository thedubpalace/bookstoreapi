package com.peeratorn.bookstore.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class BookService {

	@Autowired
	public BookService() {
	}

	public String getBooks() {
		final String uri = "https://scb-test-book-publisher.herokuapp.com/books";
		return restRequest(uri);
	}
	
	public String getRecommendBooks() {
		final String uri = "https://scb-test-book-publisher.herokuapp.com/books/recommendation";
		return restRequest(uri);
	}

	private String restRequest(final String uri) {
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(uri, String.class);
		return result;
	}
	

}