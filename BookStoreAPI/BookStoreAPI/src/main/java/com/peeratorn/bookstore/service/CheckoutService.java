package com.peeratorn.bookstore.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peeratorn.bookstore.entity.Order;

@Service
public class CheckoutService {

	@Autowired
	private BookService bookService;
	
	@Autowired
	public CheckoutService() {
	}

	public double calculatePrice(Order order) {
		HashMap<Long, Double> priceMap = getPriceMap();

		double totalPrice = 0;
		for (Long id : order.getOrders()) {
			totalPrice += priceMap.get(id).doubleValue();
		}
		return totalPrice;
	}

	private HashMap<Long, Double> getPriceMap() {
		String booksJsonString = bookService.getBooks();
		HashMap<Long, Double> pricemap = new HashMap<>();
		List<HashMap<String, Object>> booksHashmapList = getBookHashMapList(booksJsonString);
		for (HashMap<String, Object> hashMap : booksHashmapList) {
			Long id = Long.parseLong(String.valueOf(hashMap.get("id")));
			Double price = Double.parseDouble(String.valueOf(hashMap.get("price")));
			pricemap.put(id, price);
		}
		return pricemap;
	}

	private List<HashMap<String, Object>> getBookHashMapList(String booksJsonString) {
		ObjectMapper mapper = new ObjectMapper();
		List<HashMap<String, Object>> dataAsMap = null;
		try {
			dataAsMap = mapper.readValue(booksJsonString, List.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataAsMap;
	}
}