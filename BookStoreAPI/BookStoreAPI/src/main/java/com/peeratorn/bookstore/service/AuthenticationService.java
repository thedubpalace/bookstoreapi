package com.peeratorn.bookstore.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peeratorn.bookstore.entity.UserDetails;
import com.peeratorn.bookstore.repository.LoginRepository;
import com.peeratorn.bookstore.security.InMemmoryUserDetails;

@Service
public class AuthenticationService {

	private LoginRepository loginRepository;
	private InMemmoryUserDetails userDetailCache;
	
	@Autowired
	public AuthenticationService(LoginRepository repository) {
		this.loginRepository = repository;
		this.userDetailCache = new InMemmoryUserDetails();
	}

	public String login(UserDetails user) {
		Optional<UserDetails> validUser = loginRepository.findByUsername(user.getUsername());
		if (!validUser.isPresent())
			return null;
		String token = userDetailCache.save(validUser.get());
		return token;
	}

	public UserDetails validate(String token) {
		Optional<UserDetails> validUser = userDetailCache.find(token);
		if (!validUser.isPresent())
			return null;
		return validUser.get();
	}
	
	public UserDetails logoff(String token) {
		Optional<UserDetails> validUser = userDetailCache.delete(token);
		if (!validUser.isPresent())
			return null;
		return validUser.get();
	}
	
}
