package com.peeratorn.bookstore.entity;


import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
@Entity
@Table(name = "Users")
public class User {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String surname;
    
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Order books;
    
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @JsonIgnore
    private UserDetails userDetails;

    @NotNull
    @Column(name="DATE_OF_BIRTH")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateOfBirth;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}


	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;

	}
	public Order getBooks() {
		return books;
	}

	public void setBooks(Order books) {
		this.books = books;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", surname=" + surname + ", books=" + books + ", dateOfBirth="
				+ dateOfBirth + "]";
	}

	public String getUsername() {
		String username = null;
		if (userDetails != null) {
			username = userDetails.getUsername();
		}
		return username;
	}
	
	public void setUsername(String username) {
		if (userDetails == null) {
			userDetails = new UserDetails();
			userDetails.setUser(this);
		}
		userDetails.setUsername(username);
	}

	public String getPassword() {
		String password = null;
		if (userDetails != null) {
			password = userDetails.getPassword();
			
		}
		return password;
	}
	
	public void setPassword(String password) {
		if (userDetails == null) {
			userDetails = new UserDetails();
			userDetails.setUser(this);
		}
		userDetails.setPassword(password);
	}

}
