package com.peeratorn.bookstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.peeratorn.bookstore.entity.User;
import com.peeratorn.bookstore.repository.UserRepository;

@Service
public class UserService {

	private UserRepository userRepository;

	@Autowired
	public UserService(UserRepository repository) {
		this.userRepository = repository;
	}

	public List<User> getUser() {
		List<User> users = new ArrayList<>();
		userRepository.findAll().forEach(e -> users.add(e));
		return users;
	}

    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
