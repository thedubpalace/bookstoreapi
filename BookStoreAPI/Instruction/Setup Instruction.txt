Setup Instruction
1. Install gradle (you can download from https://gradle.org/releases/)
2. Install MySQL
3. run SQL file in DB Schema\SCB_DB.sql to create schema
4. Configure DB url and credential in BookStoreAPI\src\main\resources\application.properties
5. Run Application by using "gradle BootRun" command