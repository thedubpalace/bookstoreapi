CREATE SCHEMA `scb_db` ;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `date_of_birth` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8


CREATE TABLE `user_details` (
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

CREATE TABLE `orders` (
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8

CREATE TABLE `order_orders` (
  `order_user_id` bigint(20) NOT NULL,
  `orders` bigint(20) DEFAULT NULL,
  KEY `FKjt33hbcly9x7txeam8afgt0gp` (`order_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8